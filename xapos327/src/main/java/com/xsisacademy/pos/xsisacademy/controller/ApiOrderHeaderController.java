package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.OrderHeader;
import com.xsisacademy.pos.xsisacademy.repository.OrderDetailRepository;
import com.xsisacademy.pos.xsisacademy.repository.OrderHeaderRepository;

@RestController
@RequestMapping("/api/transaction/")
public class ApiOrderHeaderController {

	@Autowired
	private OrderHeaderRepository orderHeaderRepository;
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@PostMapping("orderheader/create")
	public ResponseEntity<Object> createReference() {
		OrderHeader orderHeader = new OrderHeader();

		String timeDec = String.valueOf(System.currentTimeMillis());

		orderHeader.reference = timeDec;
		orderHeader.amount = 0;
		orderHeader.createBy = "admin1";
		orderHeader.isActive = true;
		orderHeader.createDate = new Date();

		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);

		if (orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Create Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Create Failed", HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxOrderHeader(){
		try {
			Long maxId = this.orderHeaderRepository.findByMaxId();
			
			return new ResponseEntity<>(maxId, HttpStatus.OK);
			
		}
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("checkout/{headerId}/{totalAmount}")
	public ResponseEntity<Object> checkout(@PathVariable("headerId") Long headerId, @PathVariable("totalAmount") double totalAmount){
		try {
			OrderHeader orderHeaderData = this.orderHeaderRepository.findById(headerId).orElse(null);
			
			orderHeaderData.setAmount(totalAmount);
			orderHeaderData.setModifyBy("admin1");
			
			this.orderHeaderRepository.save(orderHeaderData);
			
			return new ResponseEntity<>("CheckOut Success" ,HttpStatus.OK);
			
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/checkhistory")
	public ResponseEntity<Object> getOrderHistory(){
		try {
			List<OrderHeader> listOrderHeader = this.orderHeaderRepository.findAllOrderHeaderCheckout();
			return new ResponseEntity<>(listOrderHeader, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	

}
