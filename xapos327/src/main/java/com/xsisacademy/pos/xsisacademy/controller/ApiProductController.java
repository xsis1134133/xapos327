package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Product;
import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.ProductRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/api/")
public class ApiProductController {

	@Autowired
	private ProductRepository productRepositoty;
	
	@Autowired
	private VariantRepository variantRepository;

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			List<Product> listProduct = this.productRepositoty.findByIsActive(true);
			//this.productRepositoty
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
			
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("product/variantbycategoryid/{categoryId}")
	public ResponseEntity<List<Variant>> getVariantByCategoryId(@PathVariable("categoryId")Long categoryId){
		try {
			List<Variant> listVariant = this.variantRepository.findByCategoryId(categoryId);
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.createBy = "admin1";
		product.createDate = new Date();
		
		Product productData = this.productRepositoty.save(product);
		
		if(productData.equals(product)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable Long id){
		try {
			Optional<Product> product = this.productRepositoty.findById(id);
			return new ResponseEntity<>(product, HttpStatus.OK);
			
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("product/edit/{id}")
	public ResponseEntity<Object> editProduct (@PathVariable("id") Long id, @RequestBody Product product){
		Optional<Product> productData = this.productRepositoty.findById(id);
		if(productData.isPresent()) {
			product.id =id;
			product.modifyBy = "admin1";
			product.modifyDate = new Date();
			product.createBy = productData.get().createBy;
			product.createDate = productData.get().createDate;
			
			this.productRepositoty.save(product);
			return new ResponseEntity<>("Update Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("product/delete/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id")Long id){
		Optional<Product> productData = this.productRepositoty.findById(id);
		
		if(productData.isPresent()) {
			Product product = new Product();
			product.id = id;
			product.setIsActive(false);
			product.setModifyBy("admin1");
			product.setModifyDate(new Date());
			product.setCreateBy(productData.get().createBy);
			product.setCreateDate(productData.get().createDate);
			product.setDescription(productData.get().description);
			product.setPrice(productData.get().price);
			product.setStock(productData.get().stock);
			product.setProductInitial(productData.get().productInitial);
			product.setProductName(productData.get().productName);
			product.setVariantId(productData.get().variantId);
			
			this.productRepositoty.save(product);
			return new ResponseEntity<>("Delete Data Succes", HttpStatus.OK);
			
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}

}
