package com.xsisacademy.pos.xsisacademy.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.VMCategory;
import com.xsisacademy.pos.xsisacademy.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/img";

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index.html");

		// Ambil data dari database
		List<Category> listCategory = this.categoryRepository.findAll();

		// lempar data ke view
		view.addObject("listCategory", listCategory);

		return view;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform.html");

		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

//	@PostMapping("save") // untuk create
//	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
//		if (!result.hasErrors()) {
//			if (category.id == null) {
//				category.createBy = "admin1";
//				category.createDate = new Date();
//			} else {
//				Category tempCategory = this.categoryRepository.findById(category.id).orElse(null);
//				if (tempCategory != null) {
//					category.createBy = tempCategory.createBy;
//					category.createDate = tempCategory.createDate;
//					category.modifyBy = "admin1";
//					category.modifyDate = new Date();
//				}
//			}
//			this.categoryRepository.save(category);
//			return new ModelAndView("redirect:/category/index");
//		} else {
//			return new ModelAndView("redirect:/category/index");
//		}
//
//	}

	@PostMapping("save") // untuk create
	public ResponseEntity<Object> save(VMCategory vmcategory, BindingResult result) {
		if (!result.hasErrors()) {

//			UUID randomUUID = UUID.randomUUID();
//			String filename = dateTime.format(formatdate) + "_" + vmcategory.file.getOriginalFilename();

			LocalDateTime dateTime = LocalDateTime.now();
			DateTimeFormatter formatdate = DateTimeFormatter.ofPattern("yyyyMMHHmmss");

			String filename = dateTime.format(formatdate) + "_" + vmcategory.file.getOriginalFilename(); // untuk
																											// memunculkan
																											// file
																											// dengan
																											// formatnya
			Path fileNameAndPath = Paths.get(uploadDirectory, filename);

			try {
				Files.write(fileNameAndPath, vmcategory.file.getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}

			Category category = new Category();

			if (vmcategory.id == null) {
				category.categoryInitial = vmcategory.categoryInitial;
				category.categoryName = vmcategory.categoryName;
				category.isActive = vmcategory.isActive;
				category.createBy = "admin1";
				category.createDate = new Date();
				category.sPhoto = filename;
			} else {
				Category tempCategory = this.categoryRepository.findById(vmcategory.id).orElse(null);
				if (tempCategory != null) {
					category = tempCategory;
					category.categoryInitial = vmcategory.categoryInitial;
					category.categoryName = vmcategory.categoryName;
					category.sPhoto = filename;
					category.modifyBy = "admin1";
					category.modifyDate = new Date();
				}
			}
			this.categoryRepository.save(category);
			return new ResponseEntity<>(category, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("checknameIsExist")
	public ResponseEntity<Boolean> checknameIsExist(@RequestParam("categoryName") String categoryName,
			@RequestParam("id") Long id) {
		
		Boolean isExist = false;
		
		Category category = null;
		
		if(id ==0) {
			 category = this.categoryRepository.findByIdName(categoryName);
		}else {
			 category = this.categoryRepository.findByIdNameForEdit(categoryName, id);
		}
		
		
		
		if(category != null) {
			isExist = true;
		}
		
		return new ResponseEntity<>(isExist, HttpStatus.OK);
		

	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("category/addform");

		Category category = this.categoryRepository.findById(id).orElse(null);

		view.addObject("category", category);
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("category/index");

		Category category = this.categoryRepository.findById(id).orElse(null);

		if (category != null) {
			this.categoryRepository.deleteById(id);

//			this.categoryRepository.delete(category);
		}

		return new ModelAndView("redirect:/category/index");
	}

	// USING API
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi.html");

		return view;
	}

	// USING API + PAGING
	@GetMapping("indexapi_pg")
	public ModelAndView indexapi_pg() {
		ModelAndView view = new ModelAndView("category/indexapi_pg.html");

		return view;
	}

}
