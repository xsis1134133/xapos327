package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/") //-> bernilai default
public class HomeController {
	@GetMapping("index")
	public String index() {
		return "index";
	}
	
	@GetMapping("home")
	public String home() {
		return "home.html";
	}
	
	@GetMapping("calculate")
	public String calculator() {
		return "calculate2.html";
	}

}
