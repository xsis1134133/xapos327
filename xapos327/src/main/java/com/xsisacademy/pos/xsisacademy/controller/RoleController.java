package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/role/")
public class RoleController {
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("role/indexapi");
		return view;
	}
	
//	@GetMapping("indexapi_pg")
//	public ModelAndView indexapi_pg() {
//		ModelAndView view = new ModelAndView("role/indexapi_pg.html");
//
//		return view;
//	}

}
