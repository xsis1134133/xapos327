package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;

	public interface CategoryRepository extends JpaRepository<Category, Long> {
		
		@Query(value = "select * from category where is_active = true", nativeQuery = true)//Pakai Native Query
		//@Query(value = "select r from Category r where is_active = false")//Pakai Java CLASS
		List<Category> findByCategories();
		
		// PAGING //
		@Query(value = " select * from category where lower(category_name) like lower(concat('%',?1,'%')) and is_active = ?2 order by category_name asc", nativeQuery = true)
		Page<Category> finByIsActive(String keyword, Boolean isActive, Pageable page);
	
		
		@Query(value = " select * from category where lower(category_name) like lower(concat('%',?1,'%')) and is_active = ?2 order by category_name desc", nativeQuery = true)
		Page<Category> finByIsActiveDESC(String keyword, Boolean isActive, Pageable page);
		
		@Query(value="select * from category where is_active = true and category_name =?1 limit 1",nativeQuery = true)
	    Category findByIdName(String categoryName);
		
		@Query(value =" select * from category where is_active = true and category_name = ?1 and id != ?2 ",nativeQuery = true)
		Category findByIdNameForEdit(String categoryName,Long id);
	}
