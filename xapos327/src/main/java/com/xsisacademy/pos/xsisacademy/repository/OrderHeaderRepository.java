package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long>{

	
	@Query(" select max(o.id) from OrderHeader o")//java class
	public  Long findByMaxId();
	
	@Query("select x from OrderHeader x where x.isActive = true")
	List<OrderHeader> findAllOrderHeaderCheckout();
}
