package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findByIsActive(Boolean isActive);

	// lebih dari dua parameter
//	@Query(value="select r from Product r where r.isActive = ?1 and p.productInitial = ?2 order by p.productInitial")
//	List<Product> findByIsActive(Boolean isActive, String productInitial);
}
