package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Role;



public interface RoleRepository extends JpaRepository<Role, Long>{
	
	List<Role> findByIsDelete(Boolean isActive);
	
//	@Query("select r from Role r where r.isDelete = true")
//	List<Role> findByRoleId(Long Id);
	
	@Query(value = " select * from tbl_role where lower(rolename) like lower(concat('%',?1,'%')) and is_delete = ?2 order by rolename asc", nativeQuery = true)
	Page<Role> finByIsDelete(String keyword, Boolean isDelete, Pageable page);

	
	@Query(value = " select * from tbl_role where lower(rolename) like lower(concat('%',?1,'%')) and is_delete = ?2 order by rolename desc", nativeQuery = true)
	Page<Role> finByIsDeleteDESC(String keyword, Boolean isDelete, Pageable page);
}
